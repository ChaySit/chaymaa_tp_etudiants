<?php 
  class Poneys {
      private $count = 8;

      public function getCount() {
        return $this->count;
      }

      public function setCount($number) {
        $this->count = $number;
      }

      public function removePoneyFromField($number) {
        if ($this->count < $number) {
           throw new \LogicException('The quantity of poneys cant be negative');
        }
        else {
           $this->count -= $number;
        }    
      }

      public function addPoneys($number) {
        $this->count += $number;
      }

      public function getNames() {

      }

      /*public function testNegativePoneysQuantity(){
        if ($this->count < 0) {
           throw new \LogicException('The quantity of poneys can t be negative');
        }
      }*/

     public function isPlacesDisponibles() {
        if ($this->count <= 15) {
           return true;
        } else { return false; }
      }

     
  }
?>
