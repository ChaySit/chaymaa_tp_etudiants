<?php
  //require_once 'src/Poneys.php';
  use PHPUnit\Framework\TestCase;

  class PoneysTest extends TestCase {

    protected $Poneys;

    public function setUp() {
        $this->Poneys = new Poneys();
        $this->Poneys->setCount(10);
     }

    /**
     * @dataProvider numberToRemove
     */
    public function test_removePoneyFromField($number, $expected) {
      // Setup
      //$Poneys = new Poneys();

      // Action
      $this->Poneys->removePoneyFromField($number);
      
      // Assert
      $this->assertEquals($expected, $this->Poneys->getCount());
    }

    public function numberToRemove()
    {
        return [
            [2,8],
            [3,7],
            [4,6]
        ];
    }

    public function test_addPoneyFromField() {
      // Setup
      //$Poneys = new Poneys();

      // Action
      $this->Poneys -> addPoneys(2);

      // Assert
      $this->assertEquals(12, $this->Poneys->getCount());
      $this->assertEquals(true, $this->Poneys->isPlacesDisponibles());
    }

    public function test_getNames()
    {
       $this->poneys = $this->getMockBuilder('Poneys')->getMock();
       $this->poneys
            ->expects($this->exactly(1))
            ->method('getNames') 
            ->willReturn(
                  array('Joe', 'William', 'Jack', 'Averell')
              );
       $this->assertEquals(
              array('Joe', 'William', 'Jack', 'Averell'),
              $this->poneys->getNames()
       );
    }

    /*public function tearDown() {
        $this->Poneys = null;
        $this->assertEquals(0, $this->Poneys->getCount());  
     }*/
  }
 ?>
