QUnit.test( "hello test", function( assert ) {
  assert.ok( 1 == "1", "Passed!" );
});


QUnit.test( "pair()", function( assert ) {
  // To ensure that an explicit number of assertions are run within any test, in this case : 6
  assert.expect( 6 );

  //var result = pair( 20 );

  assert.equal( pair(20), true, "20 est un nombre pair" );
  assert.equal( pair(1), false, "1 n'est pas un nombre pair" );
  assert.equal( pair(3), false, "3 n'est pas un nombre pair" );
  assert.equal( pair(6), true, "6 est un nombre pair" );
  assert.equal( pair(4), true, "4 est un nombre pair" );
  assert.equal( pair(15), false, "15 n'est pas un nombre pair" );

});

QUnit.test( "Bonjour test ", function( assert ) {

  assert.equal( bonjour("Chaymaa"), "Bonjour Chaymaa" , "Passed :D " );

});
